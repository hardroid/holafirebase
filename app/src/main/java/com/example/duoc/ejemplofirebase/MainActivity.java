package com.example.duoc.ejemplofirebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference databaseReference;
    private RecyclerView rvUsuarios;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvUsuarios = (RecyclerView)findViewById(R.id.rvUsuarios);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("usuarios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Usuario> dataSet = new ArrayList<>();

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Usuario usuario = postSnapshot.getValue(Usuario.class);
                    dataSet.add(usuario);
                }
                Log.d("Firebase", "Existen " + dataSet.size() + " usuarios");
                cargarListaRecycler(dataSet);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void cargarListaRecycler(final ArrayList<Usuario> values) {

        rvUsuarios.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvUsuarios.setLayoutManager(mLayoutManager);
        mAdapter = new UsuariosAdapter(values, this);

        rvUsuarios.setAdapter(mAdapter);
    }

}
