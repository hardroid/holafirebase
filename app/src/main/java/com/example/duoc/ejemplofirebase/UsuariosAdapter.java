package com.example.duoc.ejemplofirebase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Duoc on 04-11-2016.
 */
public class UsuariosAdapter extends RecyclerView.Adapter<UsuariosAdapter.ViewHolder>  {

    private ArrayList<Usuario> dataSet;
    private Context context;

    public UsuariosAdapter(ArrayList<Usuario> dataSet, Context context){
        this.dataSet = dataSet;
        this.context = context;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuarios, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Usuario usuario = dataSet.get(position);
        viewHolder.txtNombre.setText(usuario.getNombre());
        viewHolder.txtCorreo.setText(usuario.getCorreo());
        viewHolder.txtFechaNacimiento.setText(usuario.getFechaNacimiento());
        viewHolder.txtSexo.setText(usuario.getSexo());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtNombre;
        public TextView txtCorreo;
        public TextView txtFechaNacimiento;
        public TextView txtSexo;

        public ViewHolder(View v) {
            super(v);
            txtNombre = (TextView) v.findViewById(R.id.txtNombre);
            txtCorreo = (TextView) v.findViewById(R.id.txtCorreo);
            txtFechaNacimiento = (TextView) v.findViewById(R.id.txtFechaNacimiento);
            txtSexo = (TextView) v.findViewById(R.id.txtSexo);
        }
    }
}
